# muterces

Blockchain application written in rust that enables users to send messages, files, crypto, voip, and email 

## TODO

- [ ] Design 
- [ ] Establish the blockchain framework
- [ ] Create two test users
- [ ] Send a message between the users
- [ ] Build the front end for creating an account and sending a message
